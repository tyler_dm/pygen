
"""

	Room.py 
	Inherits from QRectF to represent a room of a dungeon
"""

#from PyQt4 import QtCore
from PyQt4.QtCore import QRectF, QPointF, QSizeF

class RoomType(object):

	def __init__(self):
		super().__init__()

		self.__basic_room = 0
		self.__hallway    = 1
		self.__main_room  = 2

	def getMainRoom(self):
		return self.__main_room

	def getHallway(self):
		return self.__hallway

	def getBasicRoom(self):
		return self.__basic_room

	MAIN_ROOM = property (fget = getMainRoom)
	HALLWAY = property (fget = getHallway)
	BASIC = property (fget = getBasicRoom)


class Room(QRectF):

	def __init__(self, w=100.0, h=100.0, loc=QPointF(0,0), room_type = RoomType.BASIC):

		# call parent constructor
		super().__init__(loc, QSizeF(w, h))

		# initialize instance variables
		self.__location = loc
		
		self.__main_room = False

		self.__room_type = room_type
	
	## returns the value of the main room flag
	## Whether or not a room is a main_room will be
	##  determined by its size. Rooms that are a
	##  constant value larger than the average room
	##  size will be set as a main room
	def isMainRoom(self):
		return self.__main_room
		
	## set the main room flag to the specified status
	def setMainRoom(self, status):
		self.__main_room = status

	def getRoomType(self):
		return self.__room_type

	def setRoomType(self, room_type):
		self.__room_type = room_type