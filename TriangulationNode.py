#!/usr/bin/python


from PyQt4.QtCore import QPointF

# Node.py
# Inherit from QPointF and maintain references
#  to nearest points

class TriangulationNode(QPointF):

	def __init__(self, x, y):
		super().__init__(x, y)

		# Maintain a list of nearest nodes
		self.neighbors = []
		
		# a list of the points to which this node is connected
		self.connections = []


	## Connect this point to the specified point p by adding it to the connections list
	## Also add this point to the connections list in point p
	def connect(self, p):
	
		duplicate = False
	
		# first a sanity check to ensure a connection does not already exist
		for i in range(len(connections)):
	
			if connections[i] == p:
				duplicate = True
				
		if not duplicate:
			self.connections.append(p)
			p.connect(self)

	def move(self, x, y):
		## move the Node to the specified x,y coordinates
		self.setX(x)
		self.setY(y)

	def getNeighbors(self, p):
		## Extract nearest neighbors from a given list of points

		## first create a copy of the list
		points = p

		## sort the list by x,y distance from this Node
		self.sortNodesByXY(points)

		## Calculate nearest 3 nodes and store them
		##  in neighbors[]

	## Bubble sort the nodes 
	def sortNodesByXY(self, p):
		unordered = True

		while unordered:
			unordered = False

			for n in range(len(p)):
				if n < len(p) - 1:

					p1 = p[n]
					p2 = p[n+1]

					if p1.x() > p2.x():
						t = p1
						p[n] = p2
						p[n+1] = t
						unordered = True

					if p1.x() == p2.x():
						if p1.y() > py.x():
							t = p1
							p[n] = p2
							p[n+1] = t
							unordered = True
			

	def getNodeDistance(self, n1, n2):
		## Given 2 nodes, determine distance by converting to a line and return length
		return QLineF(n1, n2).length()
