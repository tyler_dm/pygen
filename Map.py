
"""
    Map.py 
    Represents a dungeon map composed of rooms
"""
import random, math
from Triangulation import *
from Room import Room, RoomType
from PyQt4.QtCore import QPointF, QPoint, QRectF, QRect, QSize

class Direction(object):

	def __init__(self):
		super().__init__()

		self.__north = 0
		self.__west  = 1
		self.__south = 2
		self.__east  = 3

	def getNorth(self):
		return self.__north

	def getWest(self):
		return self.__west

	def getSouth(self):
		return self.__south

	def getEast(self):
		return self.__east

	WEST  = property(fget = getWest)
	NORTH = property(fget = getNorth)
	SOUTH = property(fget = getSouth)
	EAST  = property(fget = getEast)


class Map (object):
	## The Map class contains the following member variables:
	##  __rooms:        A list of the rooms generated. Whenever generateRoom() is called,
	##                      the room is automatically appended to this list.
	##  __maxRoomSize:  Determines the maximum threshold when selecting usable rooms
	##  __minRoomSize:  Determines the minimum threshold when selecting usable rooms
	##  __grid_size:    Sets precision of object alignment on the map. All coordinates
	##                      will be in multiples of grid_size
	## __num_rooms:     Indicates how many rooms should be created

	def __init__(self, num_rooms = 20,roomSize_min = 10.0, roomSize_max = 50.0, grid_size = 5):

		super().__init__()

		self.__rooms = []
		self.__main_rooms = []
		self.__num_rooms = num_rooms
		self.__maxRoomSize = roomSize_max
		self.__minRoomSize = roomSize_min
		self.__grid_size = grid_size
		self.__triangulation = Triangulation()

		for i in range(self.__num_rooms):
			self.generateRoom()
			
		print ("Generated {} rooms".format(len(self.__rooms)))
			
		self.explode()

		#self.getAllStats()
		self.assignRoomTypes()


	def getRooms(self):
		return self.__rooms

	def getNumRooms(self):
		return self.__num_rooms
		
	def getTriangulation(self):
		return self.__triangulation

	def generateRoom(self):

		w = random.randint(self.__minRoomSize, self.__maxRoomSize)
		h = random.randint(self.__minRoomSize, self.__maxRoomSize)

		w = self.roundToGridSize(w)
		h = self.roundToGridSize(h)

		pos = self.getRandomQPointFInCirlce(25)

		self.__rooms.append(Room(w, h, pos))


	def getRandomQPointFInCirlce(self, radius):
		## Generate and return a random point within a circle.
		##  This will represent the centerpoint of a room
		t = 2 * math.pi * random.random()
		u = random.random() + random.random()
		r = None

		if u > 1:
			r = 2 - u

		else:
			r = u

		x = radius * r * math.cos(t)
		y = radius * r * math.sin(t)

		x = self.roundToGridSize(x)
		y = self.roundToGridSize(y)

		return QPointF(x,y)


	def getStats(self, room):
		print (" center: {}".format(room.center()))
		print (" top:\t{}".format(room.top()))
		print (" left:\t{}".format(room.left()))
		print (" bottom:\t{}".format(room.bottom()))
		print (" right:\t{}".format(room.right()))


	def getAllStats(self):
		for r in range(len(self.__rooms)):
			print ("Room: " + str(r))
			self.getStats(self.__rooms[r])


	def roundToGridSize(self, n):
		## n can be rounded to the nearest multiple of grid
		## using the following formula:
		##      r = round(n / __grid_size) * __grid_size

		return round(n / self.__grid_size) * self.__grid_size

	## Takes a room and a direction as parameters
	##  Moves the centerpoint of room r by __grid_size units in direction d
	def move(self, r, d):

		room = self.__rooms[r]

		dx = 0
		dy = 0

		if d == Direction.NORTH:
			dy = self.__grid_size

		elif d == Direction.SOUTH:
			dy = -(self.__grid_size)

		elif d == Direction.WEST:
			dx = -(self.__grid_size)

		elif d == Direction.EAST:
			dx = self.__grid_size

		else:
			dx = 0
			dy = 0

		c = room.center() + QPointF(dx, dy)

		room.moveCenter(c)

	## Rooms are initially generated as a stacked mess.
	##  distributeRooms will spread rooms out to limit intersections
	##  to edge-points
	## Recursively checks rooms for overlaps
	## Exit condition is no overlaps detected
	
	### Explode should look at each room and move every other room until they do not intersect this one.
	### The Direction to move should be determined by the quadrant of the circle the centerpoint lies in
	###  If the room center lies in Q1, moves can be up or right
	###				Q2, moves can be up or left
	###				Q3, moves can be down or left
	###				Q4, moves can be down or right
	###  Rooms will move in their allowed direction based on which edge of the intersecting room to which
	###   it is nearest
	
	def explode(self, found = True):
		## Loop through each room R in rooms[]
		##  create a list of rooms that intersect with room R
		##  move each intersecting room in the direction nearest to room R's edge E
		
		room_len = len(self.__rooms)
		
		while found:
		
			found = False
		
			for r1 in range(room_len):

				intersections = [] # a list of all the r2s intersecting with room
			
				print ("\nDetermining intersections for room: {}".format(r1))
			
				for r2 in range(room_len):
				
					# sanity check to be sure we are not checking a room against itself
					if not (r2 == r1):
						if self.__rooms[r2].intersects(self.__rooms[r1]):
							print ("  Intersection: {}  ".format(self.__rooms[r2]))
							intersections.append(self.__rooms[r2])
							found = True
			
				# intersections list should now be full
				# Now move each intersector accordingly
				for intersector in intersections:
				
					i = self.__rooms.index(intersector)
					
					## again check to make sure we arent looking at the same index
					##  twice
					
					if not (r1 == i):
					
						## move the room until there is no longer an intersection
						while self.__rooms[r1].intersects(self.__rooms[i]):
							edge = self.getNearestEdge(self.__rooms[r1], intersector)
							self.move(i, edge)
			self.explode(found)
					
			

	def getNearestEdge(self, room1, room2):
		## Compare the coordinates of room1 and room2
		##  Move room1 in the direction of room2's nearest edge to room1's center
		##  For example: If room1 intersects room2 such that room1's center is closest
		##  the the right edge of room 2, move room1 to the right
		## Returns a Direction
		
		## First get center coordinates for each room
		r1x = room1.center().x()
		r1y = room1.center().y()
		
		r2x = room2.center().x()
		r2y = room2.center().x()
		
		# Default to north so nothing breaks
		edge = Direction.NORTH
		
		if r2x > r1x:
			edge = Direction.EAST
		
		#elif r2x < r1x:
			#edge = Direction.WEST
		
		else:
		
			if r2y > r1y:
				edge = Direction.NORTH
	
			#elif r2y > r1y:
				#edge = Direction.SOUTH
			
		return edge
		
		
	## Calculate the average room size as a QRectF
	def getAvgRoomSize(self):
		total_width = 0.0
		total_height = 0.0
		
		total_rooms = len(self.__rooms)
		
		for room in self.__rooms:
			total_width += room.width()
			total_height += room.height()
			
		avg_width = self.roundToGridSize(total_width / total_rooms)
		avg_height = self.roundToGridSize(total_height / total_rooms)
		
		return QRectF(QRect(QPoint(0,0), QSize(avg_width, avg_height)))
		
		
	## Assigns the main rooms based on size relative to the average
	def assignRoomTypes(self):
		avg_room = self.getAvgRoomSize()
		
		for room in self.__rooms:
		
			if self.isHallway(room):
				room.setRoomType(RoomType.HALLWAY)
		
			elif room.width() > avg_room.width() and room.height() > avg_room.height():
				#room.setRoomType(RoomType.MAIN_ROOM)
				#self.addTriangulationPoint(room.center())
				
				room.setRoomType(RoomType.BASIC)

			else:
				room.setRoomType(RoomType.BASIC)
				#r = random.randint(0, 100)
				
				#if r >= 50:
				#	room.setRoomType(RoomType.HALLWAY)

				#else:
				#	room.setRoomType(RoomType.BASIC)

	
	## Determine if a room is a hallway by calculating if its width is at least 3/4 longer
	##  in one direction than the other
	def isHallway(self, room):
		hallway = False
		
		if room.width() > room.height() * 1.75:
			hallway = True
			
		elif room.height() > room.width() * 1.75:
			hallway = True
			
		return hallway
	
	def addTriangulationPoint(self, p = QPointF(0,0)):
		self.__triangulation.addPoint(p)
				
	def triangulate(self):
		self.__triangulation = Triangulation()
		
		for r in self.__rooms:
			if r.getRoomType() == RoomType.MAIN_ROOM:
				self.__triangulation.addPoint(r.center())

def main():
	_map = Map()

if __name__ == "__main__":
	main()
