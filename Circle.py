#!/usr/bin/python

## A simple circle class made of a centerpoint described as a QPointF
##  and a radius described as the distance of a QLineF to a given QPointF

from PyQt4.QtCore import QPointF, QLineF

class Circle(object):


	## r is merely a QPointF. The actual radius will be calculated
	def __init__(self, c, r):
		super().__init__()
		
		self.__center = c
		self.__radiusPoint = r
		
	def getRadius(self):
		return QLineF(self.__center, self.__radiusPoint).length()
		
	def getCenter(self):
		return self.__center
		
	
	center = property (fget = getCenter)
	radius = property (fget = getRadius)
		
	## determine if the given point lies within this circle
	def isInCircle(self, p = QPointF(0,0)):
		
		## the point p is in the circle if the distance from p is 
		##  less than the radius
		## get a QLineF.distance() for p to center and compare to
		##  radius
		
		contained = False
		
		if radius > QLineF(center, p).length():
			contained = True
		
		return contained