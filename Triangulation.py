#!/usr/bin/python

import sys
import random
from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import QPointF, QLineF, QRectF, QSizeF

class Triangulation(QtGui.QWidget):

	def __init__(self):
	
		super().__init__()
		
		self.points = []
		self.triangles = []
		self.lines = []
		
		#self.init_ui()
		
		
	def init_ui(self):
		
		self.setWindowTitle("Triangulation")
		
		
		self.resize(800, 600)
		
		self.grid = QtGui.QGridLayout()
		self.grid.setSpacing(10)
		
		self.scene = QtGui.QGraphicsScene()
		self.view = QtGui.QGraphicsView()
		
		self.view.setScene(self.scene)
		
		self.pen = QtGui.QPen(QtGui.QColor(255,0,0))
		
		self.setLayout(self.grid)
		
		self.grid.addWidget(self.view, 0, 0, 1, 2)
		
		#self.points = [QPointF(100,100),
		#		QPointF(200,200),
		#		QPointF(450,375),
		#		QPointF(100,540),
		#		QPointF(322,111),
		#		QPointF(222,333),
		#		QPointF(444,555),
		#		QPointF(123,456),
		#		QPointF(321,123),
		#		QPointF(432,234),
		#		QPointF(246,246)]
		
		
		#for i in range(10):
		#	#p = self.genRandomPoint()
		#	s = QSizeF(1,1)
			
			#self.points.append(p)
			
		#	self.scene.addEllipse(QRectF(self.points[i], s))
			
		#self.triangulate()
		
		#self.show()
	
	def addPoint(self, p):
		self.points.append(p)

	def getPoints(self):
		return self.points

	
	def genRandomPoint(self):
		rx = random.randint(100, 700)
		ry = random.randint(100, 500)
		
		return QPointF(rx, ry)
		
		#self.scene.addEllipse(rx, ry, 1, 1, self.pen)
		
	def triangulate(self):
	
		print ("Triangulating...")
		## An arguably primitive triangulation formula.
		## Sort a list of points P by ascending x value
		## Connect each point p to the next point in the list
		##  until P[i] == len(P) - 1
		## Draw a straight line to each vertex, unless a line
		##  exists between the two points already
		## If the line can be drawn without intersecting any other line,
		##  draw the line. 
		## Repeat until no lines are drawn
		
		self.bubbleSortPoints()
		self.connectSequentialPoints()
		
	
	def bubbleSortPoints(self):
		## Sorts items in a list
		print ("sorting...")
		unordered = True
		
		while  unordered:
			unordered = False
			for p in range(len(self.points)):
				if p < len(self.points) - 1:
				
					if self.points[p].x() > self.points[p + 1].x():
						print ("Swapping points: ({},{}) <--> ({},{})".format(self.points[p].x(), self.points[p].y(),
													self.points[p + 1].x(), self.points[p + 1].y()))
						
						t = self.points[p + 1]
					
						self.points[p + 1] = self.points[p]
						self.points[p] = t 
						
						unordered = True
						
		print ("Done.")
		for p in self.points:
			print ("({},{})".format(p.x(),p.y()))
			
	def connectSequentialPoints(self):
		
		for p in range(len(self.points)):
			if p < len(self.points) - 2:
				
				p1 = self.points[p]
				p2 = self.points[p + 1]
				p3 = self.points[p + 2]
				
				line = QLineF(p1, p2)
				
				self.lines.append(line)
				
				self.scene.addLine(line, QtGui.QPen(QtGui.QColor(255,0,0)))
				
				next_line = QLineF(p1, p3)
				
				for x in range(len(self.lines)):
				
					ip = QPointF(0,0)
					
					i = next_line.intersect(self.lines[x], ip)
					print (i)
				
					if (i == QLineF.UnboundedIntersection):
						self.lines.append(next_line)
						self.scene.addLine(next_line, QtGui.QPen(QtGui.QColor(0,0,255)))
						
		
def main():
	app = QtGui.QApplication(sys.argv)
	t = Triangulation()
	t.show()
	sys.exit(app.exec_())
	
	
	
	
if __name__ == "__main__":
	main()
