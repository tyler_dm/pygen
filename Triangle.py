
## A Simple Triangle Class
## Triangle Simply contains 3 QPointFs represented as a, b, and c
##  Sides could be calculated
##  Triangle will need to be able to find they circumcircle of the set
##   of points

from TriangulationNode import TriangulationNode
from Circle import Circle
from PyQt4.QtCore import QPointF, QLineF

class Triangle(object):

	def __init__(self, a = TriangulationNode(0,0), b = TriangulationNode(100,0), c = TriangulationNode(100,0)):
				
		super().__init__()

		self.__node_A = a
		self.__node_B = b
		self.__node_C = c
		
		self.circle = self.calcCircumcircle()


	def getNodeA(self):
		return self.__node_A

	def setNodeA(self, a):
		self.__node_A = a

	def getNodeB(self):
		return self.__node_B

	def setNodeB(self, b):
		self.__node_B = b

	def getNodeC(self):
		return self.__node_C

	def setNodeC(self, c):
		self.__node_C = c

	A = property (fget = getNodeA, fset = setNodeA)
	B = property (fget = getNodeB, fset = setNodeB)
	C = property (fget = getNodeC, fset = setNodeC)

	def getNodes(self):
		return [TriangulationNode(self.__node_A),
				TriangulationNode(self.__node_B),
				TriangulationNode(self.__node_C)
				]

	def setNodes(self, n1, n2, n3):
		self.__node_A = n1
		self.__node_B = n2
		self.__node_C = n3
		
		self.circumcircle = self.calcCircumcircle()
		
	## Return a tuple containing the centerpoint and radius of this triangle's
	##  circumcircle
	def calcCircumcircle(self):
		c = self.calcCircumcircleCenter()
		r = QLineF(c, self.__node_A)
		
		return (c, r)

	## Calculate the circumcircle of the triangle
	def calcCircumcircleCenter(self):

		## Find the perpendicular bisectors of the Triangle Nodes
		mid_ab = self.calcMidPoint(self.__node_A, self.__node_B)
		mid_ac = self.calcMidPoint(self.__node_A, self.__node_C)
		mid_bc = self.calcMidPoint(self.__node_B, self.__node_C)

		## Create lines to represent the edges of the triangle
		line_ab = QLineF(self.__node_A, self.__node_B)
		line_ac = QLineF(self.__node_A, self.__node_C)
		line_bc = QLineF(self.__node_B, self.__node_C)

		## Create lines with the opposite slope of the edges to create their
		##  perpendicular bisectors
		bisector_ab = self.calcBisector(line_ab)
		bisector_ac = self.calcBisector(line_ac)
		bisector_bc = self.calcBisector(line_bc)
		
		# initialize empty points to catch the intersectionPoint from QLineF.intersect()
		ipoint_ab_ac = QPointF()
		ipoint_ab_bc = QPointF()
		ipoint_ac_bc = QPointF()
		
		itype_ab_ac = bisector_ab.intersect(bisector_ac, ipoint_ab_ac)
		itype_ab_bc = bisector_ab.intersect(bisector_bc, ipoint_ab_bc)
		itype_ac_bc = bisector_ac.intersect(bisector_bc, ipoint_ac_bc)
		
		## if all three intersection points are the same, then we've found the circumcircle centerpoint
		##  reallistically we probably only need two points, but using all three feels safer somehow
		if ipoint_ab_ac == ipoint_ab_bc and ipoint_ab_bc == ipoint_ac_bc:
			c = ipoint_ab_ac
		
		return c
		
	## Given a line, calculate the perpendicular bisector by finding the inverse delta
	##  of the given line
	## The perpendicular bisector is a 
	def calcBisector(self, line):
		p1 = line.p1()
		p2 = line.p2()
	
		# find slope of the original line	
		dx = p1.x() - p2.x()
		dy = p1.y() - p2.y()
		
		midpt = self.calcMidPoint(p1, p2)
		
		# invert slope	
		b_dx = -dy
		b_dy = -dx
		
		# create the endpoint 
		b_end = QPointF(b_dx, b_dy) + midpt
		
		return QLineF(midpt, b_end)
	
	## Calculate the midpoint given two nodes
	def calcMidPoint(self, n1, n2):
		mid_x = (n1.x() + n2.x()) / 2
		mid_y = (n1.y() + n2.y()) / 2

		return QPointF(mid_x, mid_y)
		

def main():
	t = Triangle()
	
	
		
if __name__ == "__main__":
	main()


