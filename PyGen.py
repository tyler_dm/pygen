#!/usr/bin/python

import sys, random, time
from PyQt4 import QtGui, QtCore

from Map import Map
from Room import RoomType

class PyGen(QtGui.QWidget):
	
	def __init__ (self):
		super().__init__()

		self.__WINDOW_WIDTH  = 1024
		self.__WINDOW_HEIGHT = 768

		self.init_ui()
		self.init_map()

		#self.drawRooms()
		#self.update()
		self.drawRooms()
		#self.drawTriangulationPoints()


	def init_map(self):   # rooms, min, max, gridsize, radius
		## room min and max should be set to a multiple of grid_size
		self.__map = Map(self.__spnNumRooms.value(),
				self.__spnSizeMin.value(),
				self.__spnSizeMax.value(),
				self.__spnGridSize.value(),
				)
		
		#rooms = self.__map.getRooms()
		#self.__numRoomsToDraw = len(rooms)
		
		#for r in rooms:
		#	self.drawSingleRoom(r)
		#	self.__numRoomsToDraw -= 1
		

	def getNumRoomsToDraw(self):
		return self.__numRoomsToDraw

	def getDimensions():
		return (self.__WINDOW_WIDTH, self.__WINDOW_HEIGHT)

	def init_ui(self):
		self.setWindowTitle('PyGen')
		self.resize(self.__WINDOW_WIDTH, self.__WINDOW_HEIGHT)

		self.__grid = QtGui.QGridLayout()
		self.__grid.setSpacing(10)
		
		self.init_scene()
		self.init_graphicsView()
		
		self.init_buttons()
		self.init_user_input()

		self.setLayout(self.__grid)

		self.show()
	
	def init_scene(self):
		self.__scene = QtGui.QGraphicsScene()
		self.__scene.setBackgroundBrush(QtGui.QColor(0,0,0))
	

	def init_graphicsView(self):
		self.__gview = QtGui.QGraphicsView()

		self.__grid.addWidget(self.__gview, 0, 0, 1, 4)

		self.__mainRoomColor = QtGui.QColor(0,100,200)
		self.__normalRoomColor = QtGui.QColor(255,255,255)

		self.__normalRoomBorderColor = QtGui.QColor(0,0,0)
		self.__hallwayBorderColor = QtGui.QColor(255,255,255)
		
		self.__pen = QtGui.QPen()
		self.__pen.setColor(QtGui.QColor(200,200,200))
		
		self.__brush = QtGui.QBrush(self.__normalRoomColor)
		#self.__scene.setForegroundBrush(self.__brush)

		self.__gview.setScene(self.__scene)

	def init_buttons(self):
		self.__btnGenerate = QtGui.QPushButton("Generate", self)
		self.__grid.addWidget(self.__btnGenerate, 3, 3, 1, 1)

		self.__btnGenerate.clicked.connect(self.genDungeon)

		#self.__btnUpdate = QtGui.QPushButton("Update...", self)
		#self.__grid.addWidget(self.__btnUpdate, 1, 3)

		#self.__btnUpdate.clicked.connect(self.update)
		
		self.__btnExport =QtGui.QPushButton("Export...", self)
		self.__grid.addWidget(self.__btnExport, 3, 2, 1, 1)
		
		self.__btnExport.clicked.connect(self.exportToImage)
		
	def init_user_input(self):
		## Initializes the spinboxes and labels for num_rooms, min_size, max_size, and grid_size
		## min/max_size MUST be a multiple of grid_size
		
		self.__grid.addWidget(QtGui.QLabel("Rooms:"), 1, 0)
		self.__spnNumRooms = QtGui.QSpinBox()
		self.__spnNumRooms.setRange(0, 1000)
		self.__grid.addWidget(self.__spnNumRooms, 1, 1)
		
		self.__grid.addWidget(QtGui.QLabel("Grid Size:"), 2, 0)
		self.__spnGridSize = QtGui.QSpinBox()
		self.__spnGridSize.setRange(1,25)
		self.__grid.addWidget(self.__spnGridSize, 2, 1)
		
		self.__grid.addWidget(QtGui.QLabel("Size Min.:"), 1, 2)
		self.__spnSizeMin = QtGui.QSpinBox()
		self.__spnSizeMin.setRange(1, 50)
		self.__grid.addWidget(self.__spnSizeMin, 1, 3)
		
		self.__grid.addWidget(QtGui.QLabel("Size Max.:"), 2, 2)
		self.__spnSizeMax = QtGui.QSpinBox()
		self.__spnSizeMax.setRange(1, 100)
		self.__grid.addWidget(self.__spnSizeMax, 2, 3)
		
		self.__spnGridSize.valueChanged.connect(self.spinGridSizeChanged)
		self.__spnSizeMax.valueChanged.connect(self.spinSizeMaxChanged)
		self.__spnSizeMin.valueChanged.connect(self.spinSizeMinChanged)
		
		self.setDefaults()
	
	def roundToGridSize(self, n):
		## n can be rounded to the nearest multiple of grid
		## using the following formula:
		##      r = round(n / __grid_size) * __grid_size

		gs = self.__spnGridSize.value()

		return round(n / gs) * gs
	
	def spinGridSizeChanged(self):
		
		min_ = self.__spnSizeMin.value()
		max_ = self.__spnSizeMax.value()
		grid = self.__spnGridSize.value()

		self.spinSizeMinChanged()
		
		self.__spnSizeMin.setValue(self.roundToGridSize(min_))
		self.__spnSizeMax.setValue(self.roundToGridSize(max_))
	
	def spinSizeMinChanged(self):
	
		min_ = self.__spnSizeMin.value()
		max_ = self.__spnSizeMax.value()
		grid = self.__spnGridSize.value()
		
		#if min_ >= self.__spnSizeMax.value():
		#	self.__spnSizeMin.setValue(max_ - grid)
			#self.__spnSizeMin.setRange(1, self.__spnSizeMax.value() - self.__spnGridSize.value())

			
		self.__spnSizeMin.setValue(self.roundToGridSize(min_))
		self.__spnSizeMin.setRange(grid, max_ - grid)
		self.__spnSizeMin.setSingleStep(grid)
	
	def spinSizeMaxChanged(self):
	
		min_ = self.__spnSizeMin.value()
		max_ = self.__spnSizeMax.value()
		grid = self.__spnGridSize.value()
			
		self.__spnSizeMax.setValue(self.roundToGridSize(max_))
		self.__spnSizeMax.setRange(min_ + grid, 100)
		self.__spnSizeMax.setSingleStep(grid)
			
		
	def setDefaults(self):
		self.__spnNumRooms.setValue(50)
		self.__spnGridSize.setValue(10)
		self.__spnSizeMin.setValue(10)
		self.__spnSizeMax.setValue(60)
		
		#self.__spnSizeMax.setSingleStep(self.__spnGridSize.value())
		#self.__spnSizeMin.setSingleStep(self.__spnGridSize.value())
		
		gs = self.__spnGridSize.value()
		
		self.__spnSizeMax.setMinimum(self.__spnSizeMin.value() + gs)
		self.__spnSizeMin.setMaximum(self.__spnSizeMax.value() - gs)
		

	def genDungeon(self):
	
		gs = self.__spnGridSize.value()
		
		mi = self.roundToGridSize(self.__spnSizeMin.value() + gs)
		ma = self.roundToGridSize(self.__spnSizeMax.value() - gs)
		
		self.__spnSizeMax.setMinimum(mi)
		self.__spnSizeMin.setMaximum(ma)
		
		self.init_map()
		self.__scene.clear()
		self.drawRooms()
		#self.drawTriangulationPoints()
		
		
	## Export the current scene to an image using a file dialog
	def exportToImage(self):
		# Create a QPixmap to hold the image data from our scene
		pixmap = QtGui.QPixmap.grabWidget(self.__gview)
		
		save_file = QtGui.QFileDialog.getSaveFileName(self, "Export to Image")
		
		pixmap.save(save_file)
		
	

	def drawRooms(self):
		print ("drawing rooms...", end="")
		self.clearScene()
		rooms = self.__map.getRooms()

		for i in range(len(rooms)):

			room_type = rooms[i].getRoomType()

			if room_type == RoomType.MAIN_ROOM:
				#self.useBrush("MAIN")
				self.useBrush("BASIC")
			
			elif room_type == RoomType.HALLWAY:
				self.useBrush("HALLWAY")

			elif room_type == RoomType.BASIC:
				self.useBrush("BASIC")

			self.__scene.addRect(rooms[i], self.__pen, self.__brush)

		print ("Done.")
			
	def drawTriangulationPoints(self):
	
		tri = self.__map.getTriangulation()
	
		points = tri.getPoints()
		triPen = QtGui.QPen(QtGui.QColor(200,0,0))
		triBrush = QtGui.QBrush(QtGui.QColor(200,0,0))
		
		for p in points:
			x = p.x() - 2
			y = p.y() - 2
			width = 4
			height = 4 
			self.__scene.addEllipse(x, y, width, height, triPen, triBrush)
			
			
	def useBrush(self, room_type = "BASIC"):
		
		if room_type.upper() == "MAIN":
			 self.__brush = QtGui.QBrush(self.__mainRoomColor)
			 self.__pen = QtGui.QPen(self.__normalRoomBorderColor)
		
		elif room_type.upper() == "BASIC":
			self.__brush = QtGui.QBrush(self.__normalRoomColor)
			self.__pen = QtGui.QPen(self.__normalRoomBorderColor)

		elif room_type.upper() == "HALLWAY":
			self.__brush = QtGui.QBrush(self.__normalRoomColor)
			self.__pen = QtGui.QPen(self.__hallwayBorderColor)


	def clearScene(self):
		self.__scene.clear()
		self.init_scene()
		self.__gview.setScene(self.__scene)

def main():

	app = QtGui.QApplication(sys.argv)
	pd = PyGen()
	sys.exit(app.exec_())

if __name__ == "__main__":
	main()
